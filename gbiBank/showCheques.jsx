import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Table } from "react-bootstrap";
import queryString from "query-string";
import http from "./httpService";
import LeftPanel from "./leftPanel";
class showCheques extends Component {
  state = {
    data: {},
    banks: [],
    amounts: [
      { display: "<10000", value: "<10000" },
      { display: ">=10000", value: ">10000" },
    ],
  };
  async fetchdata() {
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    let response = await http.get(`/getAllCheques?${searchStr}`);
    let banks = await http.get(`/getBanks`);
    let { data } = response;
    this.setState({ data: data, banks: banks.data });
    console.log(response);
  }
  componentDidMount() {
    this.fetchdata();
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) this.fetchdata();
  }
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    let { page = "1" } = queryParams;
    let newPage = +page + incr;
    queryParams.page = newPage;
    this.callURL("/allCheque", queryParams);
  };
  handleOptionChange = (options) => {
    options.page = 1;
    this.callURL("/allCheque", options);
  };
  callURL = (url, options) => {
    let searchStr = this.makeSearchString(options);
    this.props.history.push({ pathname: url, search: searchStr });
  };
  makeSearchString = (options) => {
    let { page, amount, bank } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "page", page);
    searchStr = this.addToQueryString(searchStr, "bank", bank);
    searchStr = this.addToQueryString(searchStr, "amount", amount);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    let { banks = [], amounts, data = {} } = this.state;
    let { items = [], page = "", totalItems = "", totalNum = "" } = data;
    let queryParams = queryString.parse(this.props.location.search);
    let size = 5;
    let pageNum = page;
    let startIndex = (pageNum - 1) * size;
    let endIndex = startIndex + totalItems - 1;
    return (
      <div className="container">
        <h4 className="mt-4 mb-0">All Cheque Transactions</h4>
        <div className="row">
          <div className="col-3">
            <LeftPanel
              options={queryParams}
              banks={banks}
              amounts={amounts}
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-9">
            <p>
              {startIndex + 1} - {endIndex + 1} of {totalNum}
            </p>
            <Table striped hover>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Cheque Number</th>
                  <th>Bank Name</th>
                  <th>Branch</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                {items.map((ct, index) => (
                  <tr key={index}>
                    <td>{ct.name}</td>
                    <td>{ct.chequeNumber}</td>
                    <td>{ct.bankName}</td>
                    <td>{ct.branch}</td>
                    <td>{ct.amount}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <div className="row">
              <div className="col-2">
                {pageNum > 1 ? (
                  <button
                    className="btn btn-secondary m-1 btn-sm"
                    onClick={() => this.handlePage(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-2 offset-8 text-right">
                {totalNum > endIndex + 1 ? (
                  <button
                    className="btn btn-secondary btn-sm m-1"
                    onClick={() => this.handlePage(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default showCheques;
