import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./httpService";
class AddPayee extends Component {
  state = {
    payee: {
      name: this.props.user.name,
      bankName: "",
      payeeName: "",
      IFSC: "",
      accNumber: "",
    },
    bank: "",
    bankOpt: ["Same Bank", "Other Bank"],
    banks: [],
    errors: {},
  };
  async componentDidMount() {
    let response = await http.get("/getBanks");
    let { data } = response;
    let index = data.findIndex((bnk) => bnk === "GBI");
    data.splice(index, 1);
    this.setState({ banks: data });
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    input.name === "bank"
      ? (s1[input.name] = input.value)
      : (s1.payee[input.name] = input.value);
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let response = await http.post(url, obj);
      console.log(response);
      if (response) {
        alert("Details Added successfully");
        this.setState({
          payee: {
            name: this.props.user.name,
            bankName: "",
            payeeName: "",
            IFSC: "",
            accNumber: "",
          },
          bank: "",
          errors: {},
        });
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        this.setState({ msg: ex.response.data });
      }
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.checkErrors();
    console.log(errors);
    if (this.isvalid(errors)) {
      if (this.state.payee.bank === "Same Bank") {
        let s1 = { ...this.state };
        s1.payee.bankName = "GBI";
        this.setState(s1);
        this.postData("/addPayee", this.state.payee);
      } else this.postData("/addPayee", this.state.payee);
    } else {
      let s1 = { ...this.state };
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isvalid = (errors) => {
    console.log(errors);
    let keys = Object.keys(errors);
    console.log(keys);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  checkErrors = () => {
    let { bankName, payeeName, IFSC, accNumber } = this.state.payee;
    let json = {};
    json.payeeName = this.handleValidatePayee(payeeName);
    json.bank = this.handleValidateBank(this.state.bank);
    if (this.state.bank === "Other Bank") {
      json.IFSC = this.handleValidateIFSC(IFSC);
      json.bankName = this.handleValidateBankName(bankName);
    }
    json.accNumber = this.handleValidateAcnt(accNumber);
    return json;
  };
  handleValidateBankName = (bankName) => (!bankName ? "Enter Bank Name" : "");
  handleValidateBank = (bank) => (!bank ? "Choose applicable Bank Option" : "");
  handleValidatePayee = (payeeName) => (!payeeName ? "Enter Payee Name" : "");
  handleValidateIFSC = (IFSC) => (!IFSC ? "Enter IFSC Code" : "");
  handleValidateAcnt = (accNumber) =>
    !accNumber ? "Enter Account Number" : "";
  render() {
    let { payee, errors, bank, banks, bankOpt } = this.state;
    let { payeeName, bankName, IFSC, accNumber } = payee;
    return (
      <div className="container">
        <h4 className="mt-4 ml-2">Add Payee</h4>
        <div className="row">
          {this.makeTextField(
            "payeeName",
            payeeName,
            "Payee Name",
            "Enter Payee Name",
            errors.payeeName
          )}
          {this.makeTextField(
            "accNumber",
            accNumber,
            "Account Number",
            "Enter Payee Account Number",
            errors.accNumber
          )}
          {this.makeRadio(bankOpt, bank, "bank")}
          <br />
          {errors.bank ? (
            <span className="text-danger">{errors.bank}</span>
          ) : (
            ""
          )}
          {bank === "Other Bank" ? (
            <React.Fragment>
              {this.makeDD(
                banks,
                bankName,
                "bankName",
                "Select Bank",
                errors.bankName
              )}
              {this.makeTextField(
                "IFSC",
                IFSC,
                "IFSC Code",
                "Enter IFSC Code",
                errors.IFSC
              )}
            </React.Fragment>
          ) : (
            ""
          )}
          <div className="col-12">
            <button className="btn btn-primary m-1" onClick={this.handleSubmit}>
              Add Payee
            </button>
          </div>
        </div>
      </div>
    );
  }
  makeRadio = (arr, value, name) => {
    return (
      <React.Fragment>
        {arr.map((opt) => (
          <div className="col-12" key={opt}>
            <div className="form-check" key={opt}>
              <input
                type="radio"
                className="form-check-input"
                value={opt}
                name={name}
                checked={value === opt}
                onChange={this.handleChange}
              />
              <label className="form-check-label">{opt}</label>
            </div>
          </div>
        ))}
      </React.Fragment>
    );
  };
  makeTextField = (name, value, label, placeholder, errors = "") => {
    let edit = this.state.edit;
    return (
      <React.Fragment>
        <div className="col-12">
          <label className="font-weight-bold">
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-12">
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name={name}
              value={value}
              placeholder={placeholder}
              onChange={this.handleChange}
            />
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </React.Fragment>
    );
  };
  makeDD = (arr, value, name, valOnTop, errors) => {
    return (
      <React.Fragment>
        <div className="col-12">
          <div className="form-group">
            <select
              className="form-control"
              name={name}
              value={value}
              onChange={this.handleChange}
              onBlur={this.handleValidate}
            >
              <option value="">{valOnTop}</option>
              {arr.map((opt) => (
                <option key={opt}>{opt}</option>
              ))}
            </select>
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </React.Fragment>
    );
  };
}
export default AddPayee;
