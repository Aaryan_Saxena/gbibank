import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./httpService";
class CustomerDetails extends Component {
  state = {
    customer: {
      name: this.props.user.name,
      gender: "",
      addressLine1: "",
      state: "",
      city: "",
      dob: { date: "", month: "", year: "" },
      PAN: "",
      addressLine2: "",
    },
    states: [],
    cities: [],
    gen: ["Male", "Female"],
    added: false,
    errors: {},
    edit: false,
  };
  async getdata() {
    let { user } = this.props;
    let response = await http.get(`/getCustomer/${user.name}`);
    let statecity = await http.get(`/statecity`);
    let { data } = response;
    let arr = statecity.data.reduce(
      (acc, curr) =>
        acc.find((val) => val.stateName === curr.stateName)
          ? acc
          : [...acc, curr],
      []
    );
    let states = arr.reduce(
      (acc, curr) =>
        acc.find((val) => val === curr.stateName)
          ? acc
          : [...acc, curr.stateName],
      []
    );
    let s1 = { ...this.state };
    s1.customer = data ? data : s1.customer;
    s1.edit = data ? true : false;
    s1.statecity = arr;
    s1.states = states;
    this.setState(s1);
    if (data) this.makeDOB();
    console.log(response);
  }
  makeDOB = () => {
    let s1 = { ...this.state };
    let json = {};
    let dob = s1.customer.dob.split("-");
    json.date = dob[0];
    json.month = dob[1];
    json.year = dob[2];
    s1.customer.dob = json;
    this.setState(s1);
  };
  componentDidMount() {
    this.getdata();
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.customer[input.name] = input.value;
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let response = await http.post(url, obj);
      console.log(response);
      if (response) {
        alert(this.props.user.name + " Details Added successfully");
        this.setState({
          customer: {
            name: this.props.user.name,
            gender: "",
            addressLine1: "",
            state: "",
            city: "",
            dob: { date: "", month: "", year: "" },
            PAN: "",
            addressLine2: "",
          },
          errors: {},
          edit: true,
        });
        window.location = "/customerDetails";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        this.setState({ msg: ex.response.data });
      }
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.checkErrors();
    console.log(errors);
    if (this.isvalid(errors)) {
      let s1 = { ...this.state };
      s1.customer.dob =
        s1.customer.dob.date +
        "-" +
        s1.customer.dob.month +
        "-" +
        s1.customer.dob.year;
      this.setState(s1);
      this.postData("/customerDetails", this.state.customer);
    } else {
      let s1 = { ...this.state };
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isvalid = (errors) => {
    console.log(errors);
    let keys = Object.keys(errors);
    console.log(keys);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  checkErrors = () => {
    let { gender, state, city, dob, PAN } = this.state.customer;
    let json = {};
    json.gender = this.handleValidateGender(gender);
    json.state = this.handleValidateState(state);
    json.city = this.handleValidateCity(city);
    json.dob = this.handleValidateDob(dob);
    json.PAN = this.handleValidatePAN(PAN);
    return json;
  };
  handleValidateGender = (gender) => (!gender ? "Choose your Gender" : "");
  handleValidateState = (state) => (!state ? "Select State" : "");
  handleValidateCity = (city) => (!city ? "Select City" : "");
  handleValidateDob = (dob) =>
    !dob.date || !dob.month || !dob.year ? "Select Date Of Birth" : "";
  handleValidatePAN = (PAN) => (!PAN ? "Enter Your PAN Number" : "");

  render() {
    let {
      customer = {},
      gen,
      errors,
      msg,
      added,
      statecity = [],
      states = [],
      edit,
    } = this.state;
    let {
      name,
      gender,
      addressLine1,
      state,
      city,
      dob,
      PAN,
      addressLine2,
    } = customer;
    console.log(city);
    if (state) {
      var find = statecity.find((st) => st.stateName === state);
    }
    let cities = find ? find.cityArr : [];
    return (
      <div className="container">
        <h3 className="mt-4">Customer Details</h3>
        <div className="row">
          {this.makeRadio(gen, gender, "gender", "Gender", errors.gender)}
          <hr />
          {this.makeDD(dob, "Date of Birth", "dob", errors.dob)}
          <div className="col-12">
            <label className="font-weight-bold">PAN</label>
            <span className="text-danger">*</span>
          </div>
          <div className="col-12">
            {this.makeTextField("PAN", PAN, "Enter PAN Number", errors.PAN)}
          </div>
          <div className="col-12">
            <label className="font-weight-bold">Address</label>
          </div>
          <div className="col-5">
            {this.makeTextField(
              "addressLine1",
              addressLine1,
              "Enter Address Line1"
            )}
          </div>
          <div className="col-5">
            {this.makeTextField(
              "addressLine2",
              addressLine2,
              "Enter Address Line2"
            )}
          </div>
          <div className="col-6">
            <label className="font-weight-bold">State</label>
            <span className="text-danger">*</span>
          </div>
          <div className="col-6">
            <label className="font-weight-bold">City</label>
            <span className="text-danger">*</span>
          </div>
          <div className="col-6">
            {this.makeDropDown(
              states,
              "state",
              state,
              "Select State",
              errors.state
            )}
          </div>
          <div className="col-6">
            {this.makeDropDown(
              cities,
              "city",
              city,
              "Select City",
              errors.city
            )}
          </div>
        </div>
        {!edit ? (
          <button className="btn btn-primary m-2" onClick={this.handleSubmit}>
            Submit
          </button>
        ) : (
          ""
        )}
      </div>
    );
  }
  makeDD = (dob, label, name, errors) => {
    console.log(dob);
    let date = [];
    let month = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let year = [];
    for (let i = 1980; i <= 2021; i++) {
      year.push(i);
    }
    let num = this.state.customer.dob.month === "February" ? 29 : 31;
    for (let i = 0; i <= num; i++) {
      date.push(i);
    }
    return (
      <React.Fragment>
        <div className="col-12">
          <label className="font-weight-bold">{label}</label>
          <span className="text-danger">*</span>
        </div>
        <div className="col-4">
          {this.makeDropDown(year, name + "year", dob.year, "Year", errors)}
        </div>
        <div className="col-4">
          {this.makeDropDown(month, name + "month", dob.month, "Month")}
        </div>
        <div className="col-4">
          {this.makeDropDown(date, name + "date", dob.date, "Date")}
        </div>
      </React.Fragment>
    );
  };
  makeDropDown = (arr = [], name, value, valOnTop, errors = "") => {
    if (name === "city") {
      if (value.substring(0, 1) === " ")
        value = value.substring(1, value.length - 1);
      else value = value;
    }
    return (
      <React.Fragment>
        <select
          className="form-control"
          name={name}
          value={value}
          onChange={this.handleDD}
        >
          <option value="">{valOnTop}</option>
          {arr.map((opt, index) => (
            <option key={index}>{opt}</option>
          ))}
        </select>
        {errors ? <span className="text-danger">{errors}</span> : ""}
      </React.Fragment>
    );
  };
  handleDD = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    console.log(input.name);
    if (input.name === "dobdate") s1.customer.dob.date = input.value;
    else if (input.name === "dobmonth") s1.customer.dob.month = input.value;
    else if (input.name === "dobyear") s1.customer.dob.year = input.value;
    else s1.customer[input.name] = input.value;
    this.setState(s1);
  };
  makeRadio = (arr, values, name, label, errors = "") => {
    return (
      <React.Fragment>
        <div className="col-3">
          <label className="form-check-label font-weight-bold">
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-9">
          <div className="row">
            {arr.map((opt, index) => (
              <div className="col-4">
                <div className="form-check">
                  <input
                    type="radio"
                    className="form-check-input"
                    value={name === "amount" ? opt : opt}
                    name={name}
                    checked={values === (name === "amount" ? opt : opt)}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label">
                    {name === "amount" ? opt : opt}
                  </label>
                </div>
              </div>
            ))}
          </div>
        </div>
        {errors ? <span className="text-danger ml-4">{errors}</span> : ""}
      </React.Fragment>
    );
  };
  makeTextField = (name, value, placeholder, errors = "") => {
    let edit = this.state.edit;
    return (
      <React.Fragment>
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={this.handleChange}
          />
          {errors ? <span className="text-danger">{errors}</span> : ""}
        </div>
      </React.Fragment>
    );
  };
}
export default CustomerDetails;
