import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./httpService";
class NomineeDetails extends Component {
  state = {
    nominee: {
      name: this.props.user.name,
      gender: "",
      jointsignatory: "",
      relationship: "",
      nomineeName: "",
      dob: { date: "", month: "", year: "" },
    },
    gen: ["Male", "Female"],
    added: false,
    errors: {},
    edit: false,
  };
  async getdata() {
    let { user } = this.props;
    let response = await http.get(`/getNominee/${user.name}`);
    let { data } = response;
    let s1 = { ...this.state };
    s1.nominee = data ? data : s1.nominee;
    s1.edit = data ? true : false;
    this.setState(s1);
    if (data) this.makeDOB();
    console.log(response);
  }
  makeDOB = () => {
    let s1 = { ...this.state };
    let json = {};
    let dob = s1.nominee.dob.split("-");
    json.date = dob[0];
    json.month = dob[1];
    json.year = dob[2];
    s1.nominee.dob = json;
    this.setState(s1);
  };
  componentDidMount() {
    this.getdata();
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    input.type === "checkbox"
      ? (s1.nominee[input.name] = input.checked)
      : (s1.nominee[input.name] = input.value);
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let response = await http.post(url, obj);
      console.log(response);
      if (response) {
        alert(
          this.props.user.name +
            " Your Nominee :: " +
            this.state.nominee.nomineeName
        );
        window.location = "/nomineeDetails";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        this.setState({ msg: ex.response.data });
      }
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.checkErrors();
    console.log(errors);
    if (this.isvalid(errors)) {
      let s1 = { ...this.state };
      s1.nominee.dob =
        s1.nominee.dob.date +
        "-" +
        s1.nominee.dob.month +
        "-" +
        s1.nominee.dob.year;
      console.log(s1.nominee);
      this.setState(s1);
      console.log(this.state.nominee);
      this.postData("/nomineeDetails", this.state.nominee);
    } else {
      let s1 = { ...this.state };
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isvalid = (errors) => {
    console.log(errors);
    let keys = Object.keys(errors);
    console.log(keys);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  checkErrors = () => {
    let { gender, relationship, nomineeName, dob } = this.state.nominee;
    let json = {};
    json.gender = this.handleValidateGender(gender);
    json.relationship = this.handleValidateRelation(relationship);
    json.nomineeName = this.handleValidateNominee(nomineeName);
    json.dob = this.handleValidateDob(dob);
    return json;
  };
  handleValidateGender = (gender) => (!gender ? "Choose your Gender" : "");
  handleValidateRelation = (relationship) =>
    !relationship ? "Enter RelationShip with Account Holder" : "";
  handleValidateNominee = (nomineeName) =>
    !nomineeName ? "Enter Nominee Name" : "";
  handleValidateDob = (dob) =>
    !dob.date || !dob.month || !dob.year ? "Select Date Of Birth" : "";
  render() {
    let { nominee = {}, gen, errors, msg, added, edit } = this.state;
    let {
      name,
      gender,
      jointsignatory,
      dob,
      nomineeName,
      relationship,
    } = nominee;
    return (
      <div className="container">
        <h4>Nominee Details</h4>
        <div className="row">
          <div className="col-12">
            <label className="font-weight-bold">
              Name<span className="text-danger">*</span>
            </label>
          </div>
          <div className="col-12">
            {this.makeTextField(
              "nomineeName",
              nomineeName,
              "Enter Nominee Name",
              errors.nomineeName
            )}
          </div>
          {this.makeRadio(gen, gender, "gender", "Gender", errors.gender)}
          {this.makeDD(dob, "Date of Birth", "dob", errors.dob)}
          <hr />
          <div className="col-12">
            <hr />
            <label className="font-weight-bold">
              Relationship<span className="text-danger">*</span>
            </label>
          </div>
          <div className="col-12">
            {this.makeTextField(
              "relationship",
              relationship,
              "Enter Relationship",
              errors.relationship
            )}
          </div>
          <div className="col-12">
            <div className="form-check">
              <input
                type="checkbox"
                className="form-check-input"
                value={jointsignatory}
                name="jointsignatory"
                checked={jointsignatory || false}
                onChange={this.handleChange}
              />
              <label> Joint Signatory</label>
            </div>
          </div>
          {!edit ? (
            <button className="btn btn-primary m-2" onClick={this.handleSubmit}>
              Submit
            </button>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
  makeDD = (dob, label, name, errors) => {
    console.log(dob);
    let date = [];
    let month = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "June",
      "July",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    let year = [];
    for (let i = 1980; i <= 2021; i++) {
      year.push(i);
    }
    let num = this.state.nominee.dob.month === "Feb" ? 29 : 31;
    for (let i = 0; i <= num; i++) {
      date.push(i);
    }
    return (
      <React.Fragment>
        <div className="col-12">
          <label className="font-weight-bold">{label}</label>
          <span className="text-danger">*</span>
        </div>
        <div className="col-4">
          {this.makeDropDown(year, name + "year", dob.year, "Year", errors)}
        </div>
        <div className="col-4">
          {this.makeDropDown(month, name + "month", dob.month, "Month")}
        </div>
        <div className="col-4">
          {this.makeDropDown(date, name + "date", dob.date, "Date")}
        </div>
      </React.Fragment>
    );
  };
  makeDropDown = (arr = [], name, value, valOnTop, errors = "") => {
    return (
      <React.Fragment>
        <select
          className="form-control"
          name={name}
          value={value}
          onChange={this.handleDD}
        >
          <option value="">{valOnTop}</option>
          {arr.map((opt, index) => (
            <option key={index}>{opt}</option>
          ))}
        </select>
        {errors ? <span className="text-danger">{errors}</span> : ""}
      </React.Fragment>
    );
  };
  handleDD = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    console.log(input.name);
    if (input.name === "dobdate") s1.nominee.dob.date = input.value;
    else if (input.name === "dobmonth") s1.nominee.dob.month = input.value;
    else if (input.name === "dobyear") s1.nominee.dob.year = input.value;
    this.setState(s1);
  };
  makeRadio = (arr, values, name, label, errors = "") => {
    return (
      <React.Fragment>
        <div className="col-3">
          <label className="form-check-label font-weight-bold">
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-9">
          <div className="row">
            {arr.map((opt, index) => (
              <div className="col-4" key={index}>
                <div className="form-check">
                  <input
                    type="radio"
                    className="form-check-input"
                    value={name === "amount" ? opt : opt}
                    name={name}
                    checked={values === (name === "amount" ? opt : opt)}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label">
                    {name === "amount" ? opt : opt}
                  </label>
                </div>
              </div>
            ))}
          </div>
        </div>
        <hr />
        {errors ? <span className="text-danger">{errors}</span> : ""}
      </React.Fragment>
    );
  };
  makeTextField = (name, value, placeholder, errors = "") => {
    let edit = this.state.edit;
    return (
      <React.Fragment>
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={this.handleChange}
          />
          {errors ? <span className="text-danger">{errors}</span> : ""}
        </div>
      </React.Fragment>
    );
  };
}
export default NomineeDetails;
