import React, { Component } from "react";
class LeftPanel extends Component {
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let options = { ...this.props.options };
    options[input.name] = input.value;
    this.props.onOptionChange(options);
  };
  makeRadio = (arr, values, name, label) => {
    return (
      <div className="row">
        <div className="col-12 px-3 py-1 bg-light border py-2">
          <label className="form-check-label font-weight-bold">{label}</label>
        </div>
        <div className="col-12 p-0">
          <div className="row">
            {arr.map((opt, index) => (
              <div className="col-12" key={index}>
                <div className="form-check border py-2">
                  <input
                    type="radio"
                    className="form-check-input mx-2"
                    value={name === "amount" ? opt.value : opt}
                    name={name}
                    checked={values === (name === "amount" ? opt.value : opt)}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label mx-4">
                    {name === "amount" ? opt.display : opt}
                  </label>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  };
  render() {
    let { bank = "", amount = "" } = this.props.options;
    let { banks, amounts } = this.props;
    return (
      <div className="row mt-4 mx-2">
        <div className="col-12">
          {this.makeRadio(banks, bank, "bank", "Bank")}
        </div>
        <div className="col-12">
          <hr />
          {this.makeRadio(amounts, amount, "amount", "Amount")}
        </div>
      </div>
    );
  }
}
export default LeftPanel;
