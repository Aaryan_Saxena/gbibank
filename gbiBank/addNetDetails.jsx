import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./httpService";
class AddNetDetails extends Component {
  state = {
    netBanking: {
      name: this.props.user.name,
      payeeName: "",
      comment: "",
      amount: "",
      bankName: "",
    },
    payees: [],
    payeesName: [],
    errors: {},
  };
  async componentDidMount() {
    let response = await http.get(`/getPayees/${this.props.user.name}`);
    var payeesName = response.data.reduce(
      (acc, curr) =>
        acc.find((val) => val === curr.payeeName)
          ? acc
          : [...acc, curr.payeeName],
      []
    );
    this.setState({ payees: response.data, payeesName: payeesName });
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.netBanking[input.name] = input.value;
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let response = await http.post(url, obj);
      console.log(response);
      if (response) {
        alert("Details Added successfully");
        window.location = "/viewNet?page=1";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        this.setState({ msg: ex.response.data });
      }
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.checkErrors();
    console.log(errors);
    if (this.isvalid(errors)) {
      let s1 = { ...this.state };
      let find = s1.payees.find(
        (py) => py.payeeName === s1.netBanking.payeeName
      );
      s1.netBanking.bankName = find.bankName;
      this.setState(s1);
      this.postData("/postNet", this.state.netBanking);
    } else {
      let s1 = { ...this.state };
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isvalid = (errors) => {
    console.log(errors);
    let keys = Object.keys(errors);
    console.log(keys);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  checkErrors = () => {
    let { payeeName, amount } = this.state.netBanking;
    let json = {};
    json.payeeName = this.handleValidatePayee(payeeName);
    json.amount = this.handleValidateAmount(amount);
    return json;
  };
  handleValidatePayee = (payeeName) => (!payeeName ? "Select Payee Name" : "");
  handleValidateAmount = (amount) => (!amount ? "Enter Amount" : "");
  render() {
    let { netBanking, payees = [], errors, payeesName = [] } = this.state;
    let { payeeName, amount, comment } = netBanking;
    return (
      <div className="container">
        <h4 className="mt-4 ml-2">Net Banking Details</h4>
        <div className="row">
          {this.makeDD(
            payeesName,
            payeeName,
            "payeeName",
            "Payee Name",
            "Select Payee Name",
            errors.payeeName
          )}
          {this.makeTextField(
            "amount",
            amount,
            "Amount",
            "Enter Amount",
            errors.amount
          )}
          {this.makeTextField(
            "comment",
            comment,
            "Comment",
            "Enter Comment",
            errors.comment
          )}
          <button className="btn btn-primary ml-3" onClick={this.handleSubmit}>
            Add Cheque
          </button>
        </div>
      </div>
    );
  }
  makeDD = (arr, value, name, label, valOnTop, errors) => {
    return (
      <React.Fragment>
        <div className="col-12">
          <label className="font-weight-bold">
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-12">
          <div className="form-group">
            <select
              className="form-control"
              name={name}
              value={value}
              onChange={this.handleChange}
              onBlur={this.handleValidate}
            >
              <option value="">{valOnTop}</option>
              {arr.map((opt) => (
                <option key={opt}>{opt}</option>
              ))}
            </select>
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </React.Fragment>
    );
  };
  makeTextField = (name, value, label, placeholder, errors = "") => {
    let edit = this.state.edit;
    return (
      <React.Fragment>
        <div className="col-12">
          <label className="font-weight-bold">
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-12">
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name={name}
              value={value}
              placeholder={placeholder}
              onChange={this.handleChange}
            />
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </React.Fragment>
    );
  };
}
export default AddNetDetails;
