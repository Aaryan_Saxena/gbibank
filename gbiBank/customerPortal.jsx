import React, { Component } from "react";
import { Link } from "react-router-dom";
class CustomerPortal extends Component {
  render() {
    const { user } = this.props;
    return (
      <div className="container">
        <h3 className="text-center mt-3 text-danger">
          Welcome to GBI BANK Customer Portal
        </h3>
        <div className="text-center">
          <img src="https://media.istockphoto.com/vectors/bank-building-isolated-on-white-background-vector-illustration-flat-vector-id900791430?k=6&m=900791430&s=612x612&w=0&h=i8p6EfGRaDb86Z5dyGgURWVi--2KFYuoVjNJUHnrChk=" />
        </div>
      </div>
    );
  }
}
export default CustomerPortal;
