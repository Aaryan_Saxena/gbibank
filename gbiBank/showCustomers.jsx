import React, { Component } from "react";
import { Link } from "react-router-dom";
import auth from "./authService";
import { Table } from "react-bootstrap";
import http from "./httpService";
import queryString from "query-string";
class showCustomers extends Component {
  state = { data: {} };
  async getCustomers() {
    let queryParams = queryString.parse(this.props.location.search);
    let searchString = this.makeSearchString(queryParams);
    let response = await http.get(`/getCustomers?${searchString}`);
    console.log(response);
    let { data } = response;
    this.setState({ data: data });
  }
  componentDidMount() {
    this.getCustomers();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) this.getCustomers();
  }
  makeSearchString = (options) => {
    let { page } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "page", page);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    let { data = {} } = this.state;
    let { items = [], totalItems = "", totalNum = "" } = data;
    let queryParams = queryString.parse(this.props.location.search);
    let { page } = queryParams;
    let size = 5;
    let pageNum = +page;
    let startIndex = (pageNum - 1) * size;
    let endIndex = startIndex + totalItems-1;
    let searchString = this.makeSearchString(queryParams);
    console.log(endIndex);
    console.log(startIndex);
    return (
      <div className="container">
        <h4 className="mt-4">All Customers</h4>
        <p>
          {startIndex + 1} - {endIndex+1} of {totalNum}
        </p>
        <Table striped hover>
          <thead>
            <tr>
              <th>Name</th>
              <th>State</th>
              <th>City</th>
              <th>Pan</th>
              <th>DOB</th>
            </tr>
          </thead>
          <tbody>
            {items.map((ct, index) => (
              <tr key={index}>
                <td>{ct.name}</td>
                <td>{ct.state}</td>
                <td>{ct.city}</td>
                <td>{ct.PAN}</td>
                <td>{ct.dob}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <div className="row">
          <div className="col-2">
            {pageNum === 1 ? (
              ""
            ) : (
              <Link to={`/allCustomers?page=${pageNum - 1}`}>
                <button className="btn btn-secondary">Prev</button>
              </Link>
            )}
          </div>
          <div className="offset-8 col-2 text-right">
            {totalNum > endIndex + 1 ? (
              <Link to={`/allCustomers?page=${pageNum + 1}`}>
                <button className="btn btn-secondary">Next</button>
              </Link>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default showCustomers;
