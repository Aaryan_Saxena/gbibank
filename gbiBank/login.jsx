import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./httpService";
import auth from "./authService";
class Login extends Component {
  state = {
    user: { name: "", password: "" },
    btn: false,
    errors: {},
    msg: "",
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.user[input.name] = input.value;
    this.handleValidate(e);
    s1.btn = s1.errors.password ? false : true;
    this.setState(s1);
  };
  handleValidate = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    let { password } = s1.user;
    switch (input.name) {
      case "password":
        s1.errors.password = this.handleValidatePswd(password);
        break;
      default:
        break;
    }
    this.setState(s1);
  };
  async login(url, obj) {
    try {
      let response = await http.post(url, obj);
      console.log(response);
      auth.login(response.data);
      window.location =
        response.data.role === "manager" ? "/admin" : "/customer";
    } catch (ex) {
      if (ex.response && ex.response.status === 500) {
        console.log(ex.response);
        let msg = "Login failed. Check the Username and Password";
        this.setState({ msg: msg });
      }
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.checkErrors(this.state.user);
    console.log(errors);
    if (this.isvalid(errors)) {
      this.setState({ btn: true });
      this.login("/login", this.state.user);
    } else {
      let s1 = { ...this.state };
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isvalid = (errors) => {
    console.log(errors);
    let keys = Object.keys(errors);
    console.log(keys);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  checkErrors = (user) => {
    let { password } = user;
    let json = {};
    json.password = this.handleValidatePswd(password);
    return json;
  };
  handleValidatePswd = (password) =>
    password
      ? password.length < 7
        ? "password must be of 7 charcater"
        : ""
      : "Password is Mandatory";
  render() {
    const { name = "", password = "" } = this.state.user;
    let { errors = null, btn, msg } = this.state;
    return (
      <div className="container">
        <h3 className="text-center mt-5">Welcome To GBI Bank</h3>
        <div className="row mt-5">
          <div className="col-6 offset-3 text-center">
            <h5 className="text-danger">{msg ? msg : ""}</h5>
            <label className="font-weight-bold mx-3">User Name</label>
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                name="name"
                value={name}
                placeholder="Enter user name"
                onChange={this.handleChange}
              />
              <span className="text-muted">
                We'll never share your user name with anyone else.
              </span>
            </div>
          </div>
          <div className="col-6 offset-3 text-center">
            <label className="font-weight-bold mx-3">Password</label>
            <div className="form-group">
              <input
                type="password"
                className="form-control"
                name="password"
                value={password}
                placeholder="Password"
                onChange={this.handleChange}
              />
              {errors && (
                <span className="text-center text-danger">
                  {errors.password}
                </span>
              )}
            </div>
          </div>
        </div>
        <center>
          <button
            className="btn btn-primary my-2"
            disabled={!btn}
            onClick={this.handleSubmit}
          >
            Login
          </button>
        </center>
      </div>
    );
  }
}
export default Login;
