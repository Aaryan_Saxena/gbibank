import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./httpService";
class AddCheque extends Component {
  state = {
    cheque: { bankName: "", branch: "", amount: "", chequeNumber: "" },
    banks: [],
    errors: {},
  };
  async componentDidMount() {
    let response = await http.get("/getBanks");
    this.setState({ banks: response.data });
  }
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.cheque[input.name] = input.value;
    this.handleValidate(e);
    this.setState(s1);
  };
  handleValidate = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    let { bankName, branch, amount, chequeNumber } = s1.cheque;
    switch (input.name) {
      case "bankName":
        s1.errors.bankName = this.handleValidateBank(bankName);
        break;
      case "branch":
        s1.errors.branch = this.handleValidateBranch(branch);
        break;
      case "amount":
        s1.errors.amount = this.handleValidateAmount(amount);
        break;
      case "chequeNumber":
        s1.errors.chequeNumber = this.handleValidateCheque(chequeNumber);
        break;
      default:
        break;
    }
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let response = await http.post(url, obj);
      console.log(response);
      if (response) {
        alert("Details Added successfully");
        window.location = "/viewCheque?page=1";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        this.setState({ msg: ex.response.data });
      }
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.checkErrors();
    console.log(errors);
    if (this.isvalid(errors)) {
      this.postData("/postCheque", {
        ...this.state.cheque,
        name: this.props.user.name,
      });
    } else {
      let s1 = { ...this.state };
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isvalid = (errors) => {
    console.log(errors);
    let keys = Object.keys(errors);
    console.log(keys);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  checkErrors = () => {
    let { bankName, branch, amount, chequeNumber } = this.state.cheque;
    let json = {};
    json.bankName = this.handleValidateBank(bankName);
    json.branch = this.handleValidateBranch(branch);
    json.amount = this.handleValidateAmount(amount);
    json.chequeNumber = this.handleValidateCheque(chequeNumber);
    return json;
  };
  handleValidateBank = (bankName) => (!bankName ? "Select Bank Name" : "");
  handleValidateBranch = (branch) => {
    if (!branch) return "Branch Code is mandatory";
    else if (branch.length < 4) return "Enter 4 digit code of branch";
    else return "";
  };
  handleValidateCheque = (chequeNumber) => {
    if (!chequeNumber) return "Cheque Number is mandatory";
    else if (chequeNumber.length < 11)
      return "Enter your 11 digit Cheque Numberf";
    else return "";
  };

  handleValidateAmount = (amount) => (!amount ? "Enter Amount" : "");
  render() {
    let { cheque, banks, errors } = this.state;
    let { chequeNumber, bankName, branch, amount } = cheque;
    return (
      <div className="container">
        <h4 className="mt-4 ml-2">Deposit Cheque</h4>
        <div className="row">
          {this.makeTextField(
            "chequeNumber",
            chequeNumber,
            "Cheque Number",
            "Enter Cheque Number",
            errors.chequeNumber
          )}
          {this.makeDD(
            banks,
            bankName,
            "bankName",
            "Bank Name",
            "Select Bank",
            errors.bankName
          )}
          {this.makeTextField(
            "branch",
            branch,
            "Branch",
            "Enter Branch Code",
            errors.branch
          )}
          {this.makeTextField(
            "amount",
            amount,
            "Amount",
            "Enter Amount",
            errors.amount
          )}
          <button className="btn btn-primary ml-3" onClick={this.handleSubmit}>
            Add Cheque
          </button>
        </div>
      </div>
    );
  }
  makeDD = (arr, value, name, label, valOnTop, errors) => {
    return (
      <React.Fragment>
        <div className="col-12">
          <label className="font-weight-bold">
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-12">
          <div className="form-group">
            <select
              className="form-control"
              name={name}
              value={value}
              onChange={this.handleChange}
              onBlur={this.handleValidate}
            >
              <option value="">{valOnTop}</option>
              {arr.map((opt) => (
                <option key={opt}>{opt}</option>
              ))}
            </select>
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </React.Fragment>
    );
  };
  makeTextField = (name, value, label, placeholder, errors = "") => {
    let edit = this.state.edit;
    return (
      <React.Fragment>
        <div className="col-12">
          <label className="font-weight-bold">
            {label}
            <span className="text-danger">*</span>
          </label>
        </div>
        <div className="col-12">
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              name={name}
              value={value}
              placeholder={placeholder}
              onChange={this.handleChange}
            />
            {errors ? <span className="text-danger">{errors}</span> : ""}
          </div>
        </div>
      </React.Fragment>
    );
  };
}
export default AddCheque;
