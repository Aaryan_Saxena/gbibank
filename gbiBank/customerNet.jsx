import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Table } from "react-bootstrap";
import queryString from "query-string";
import http from "./httpService";
class CustomerNet extends Component {
  state = {
    data: {},
  };
  async fetchdata() {
    let { user } = this.props;
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    let response = await http.get(
      `/getNetBankingByName/${user.name}?${searchStr}`
    );
    let { data } = response;
    this.setState({ data: data });
    console.log(response);
  }
  componentDidMount() {
    this.fetchdata();
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) this.fetchdata();
  }
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    let { page = "1" } = queryParams;
    let newPage = +page + incr;
    queryParams.page = newPage;
    this.callURL("/viewNet", queryParams);
  };
  callURL = (url, options) => {
    let searchStr = this.makeSearchString(options);
    this.props.history.push({ pathname: url, search: searchStr });
  };
  makeSearchString = (options) => {
    let { page } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "page", page);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    let { banks = [], amounts, data = {} } = this.state;
    let { items = [], page = "", totalItems = "", totalNum = "" } = data;
    let queryParams = queryString.parse(this.props.location.search);
    let size = 5;
    let pageNum = page;
    let startIndex = (pageNum - 1) * size;
    let endIndex = startIndex + totalItems - 1;
    return (
      <div className="container">
        <h4 className="mt-4 mb-0">All Net Banking Details</h4>
        {items.length !== 0 ? (
          <React.Fragment>
            <p>
              {startIndex + 1} - {endIndex + 1} of {totalNum}
            </p>
            <Table striped hover>
              <thead>
                <tr className="text-center">
                  <th>Payee Name</th>
                  <th>Amount</th>
                  <th>Bank</th>
                  <th>Comment</th>
                </tr>
              </thead>
              <tbody>
                {items.map((ct, index) => (
                  <tr key={index} className="text-center">
                    <td>{ct.payeeName}</td>
                    <td>{ct.amount}</td>
                    <td>{ct.bankName}</td>
                    <td>{ct.comment}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <div className="row">
              <div className="col-2">
                {pageNum > 1 ? (
                  <button
                    className="btn btn-secondary m-1 btn-sm"
                    onClick={() => this.handlePage(-1)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-2 offset-8 text-right">
                {totalNum > endIndex + 1 ? (
                  <button
                    className="btn btn-secondary btn-sm m-1"
                    onClick={() => this.handlePage(1)}
                  >
                    Next
                  </button>
                ) : (
                  ""
                )}
              </div>
            </div>
          </React.Fragment>
        ) : (
          <h5 className="text-danger">No Transaction to Show</h5>
        )}
      </div>
    );
  }
}
export default CustomerNet;
