import React, { Component } from "react";
import { Link } from "react-router-dom";
import http from "./httpService";
class AddCustomer extends Component {
  state = {
    customer: { name: "", password: "", confirmpswd: "" },
    added: false,
    errors: {},
  };
  handleChange = (e) => {
    const { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1.customer[input.name] = input.value;
    this.handleValidate(e);
    this.setState(s1);
  };
  handleValidate = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    let { name, password, confirmpswd } = s1.customer;
    switch (input.name) {
      case "name":
        s1.errors.name = this.handleValidateName(name);
        break;
      case "password":
        s1.errors.password = this.handleValidatePswd(password);
        break;
      case "confirmpswd":
        s1.errors.confirmpswd = this.handleValidateCnfrmPswd(
          password,
          confirmpswd
        );
        break;
      default:
        break;
    }
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let response = await http.post(url, obj);
      console.log(response);
      if (response) {
        alert("Customers added successfully");
        window.location = "/allCustomers?page=1";
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        this.setState({ msg: ex.response.data });
      }
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    let errors = this.checkErrors(this.state.customer);
    console.log(errors);
    if (this.isvalid(errors)) {
      const { customer } = this.state;
      let json = {};
      json.name = customer.name;
      json.password = customer.password;
      this.postData("/register", json);
    } else {
      let s1 = { ...this.state };
      s1.errors = errors;
      this.setState(s1);
    }
  };
  isvalid = (errors) => {
    console.log(errors);
    let keys = Object.keys(errors);
    console.log(keys);
    let count = keys.reduce((acc, curr) => (errors[curr] ? acc + 1 : acc), 0);
    return count === 0;
  };
  checkErrors = (user) => {
    let { name, password, confirmpswd } = user;
    let json = {};
    json.name = this.handleValidateName(name);
    json.password = this.handleValidatePswd(password);
    json.confirmpswd = this.handleValidateCnfrmPswd(password, confirmpswd);
    return json;
  };
  handleValidateName = (name) => (!name ? "Name Is Mandatory" : "");
  handleValidatePswd = (password) => {
    if (!password) return "Password is mandatory";
    else if (password.length < 7)
      return "Password can not be blank, Minimum length should be 7 character";
    else return "";
  };

  handleValidateCnfrmPswd = (password, confirmpswd) => {
    if (!confirmpswd) return "Confirm password is mandatory";
    else if (password !== confirmpswd) return "Password do not match";
    else return "";
  };
  render() {
    let { name, password, confirmpswd } = this.state.customer;
    let { errors, msg, added } = this.state;
    return (
      <div className="container">
        <h3 className="mt-4"> New Customer</h3>
        <h4 className="text-danger">{msg ? { msg } : ""}</h4>
        {this.makeTextField(
          "name",
          name,
          "Name",
          "Enter The Customer Name",
          errors.name
        )}
        {this.makeTextField(
          "password",
          password,
          "Password",
          "Enter The Password",
          errors.password
        )}
        {this.makeTextField(
          "confirmpswd",
          confirmpswd,
          "Confirm Password",
          "Re-enter the Password",
          errors.confirmpswd
        )}
        <button
          className="btn btn-primary my-2"
          onClick={this.handleSubmit}
          disabled={this.handleDisable(errors)}
        >
          Create
        </button>
      </div>
    );
  }
  handleDisable = (errors) => {
    if (
      errors.name !== "" ||
      errors.password !== "" ||
      errors.confirmpswd !== ""
    )
      return true;
    else return false;
  };
  makeTextField = (name, value, label, placeholder, errors) => {
    return (
      <React.Fragment>
        <label className="font-weight-bold  mx-3">{label}</label>

        <div className="form-group">
          <input
            type={
              name === "password" || name === "confirmpswd"
                ? "password"
                : name === "email"
                ? "email"
                : "text"
            }
            className="form-control"
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={this.handleChange}
          />
          {errors ? <span className="text-danger">{errors}</span> : ""}
        </div>
      </React.Fragment>
    );
  };
}
export default AddCustomer;
