import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Navbar, Form, NavDropdown, Button, Nav } from "react-bootstrap";
class BankNavbar extends Component {
  render() {
    const { user } = this.props;
    return (
      <React.Fragment>
        <Navbar bg="warning" expand="lg">
          <Link
            to={
              user
                ? user.role === "manager"
                  ? "/admin"
                  : "/customer"
                : "/home"
            }
          >
            <Navbar.Brand>Home</Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              {user && user.role === "manager" && (
                <NavDropdown title="Customers" id="basic-nav-dropdown">
                  <NavDropdown.Item>
                    <Link to="/addCustomer" className="text-dark">
                      Add Customer
                    </Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link to="/allCustomers?page=1" className="text-dark">
                      View All Customers
                    </Link>
                  </NavDropdown.Item>
                </NavDropdown>
              )}
              {user && user.role === "manager" && (
                <NavDropdown title="Transactions" id="basic-nav-dropdown">
                  <NavDropdown.Item>
                    <Link to="/allCheque?page=1" className="text-dark">
                      Cheques
                    </Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link to="/allNet?page=1" className="text-dark">
                      Net Banking
                    </Link>
                  </NavDropdown.Item>
                </NavDropdown>
              )}
              {user && user.role === "customer" && (
                <NavDropdown title="View" id="basic-nav-dropdown">
                  <NavDropdown.Item>
                    <Link to="/viewCheque?page=1" className="text-dark">
                      Cheques
                    </Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link to="/viewNet?page=1" className="text-dark">
                      Net Banking
                    </Link>
                  </NavDropdown.Item>
                </NavDropdown>
              )}
              {user && user.role === "customer" && (
                <NavDropdown title="Details" id="basic-nav-dropdown">
                  <NavDropdown.Item>
                    <Link to="/customerDetails" className="text-dark">
                      Customer
                    </Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link to="/nomineeDetails" className="text-dark">
                      Nominee
                    </Link>
                  </NavDropdown.Item>
                </NavDropdown>
              )}
              {user && user.role === "customer" && (
                <NavDropdown title="Transaction" id="basic-nav-dropdown">
                  <NavDropdown.Item>
                    <Link to="/addPayee" className="text-dark">
                      Add Payee
                    </Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link to="/cheque" className="text-dark">
                      Cheque
                    </Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item>
                    <Link to="/netBanking" className="text-dark">
                      Net Banking
                    </Link>
                  </NavDropdown.Item>
                </NavDropdown>
              )}
            </Nav>
            <Form inline>
              {user ? <p className="mb-0 mx-3">Welcome {user.name}</p> : ""}
              {user ? (
                <Link to="/logout">
                  <Button variant="outline-light">
                    Logout <i className="fas fa-sign-in"></i>
                  </Button>
                </Link>
              ) : (
                <Link to="/login">
                  <Button variant="outline-light">
                    Login <i className="fas fa-sign-in"></i>
                  </Button>
                </Link>
              )}
            </Form>
          </Navbar.Collapse>
        </Navbar>
      </React.Fragment>
    );
  }
}
export default BankNavbar;
