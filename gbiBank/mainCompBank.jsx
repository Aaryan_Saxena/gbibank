import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Navbar from "./navbar";
import Login from "./login";
import Logout from "./logout";
import Home from "./home";
import auth from "./authService";
import ShowCustomers from "./showCustomers";
import ShowCheques from "./showCheques";
import ShowNetBankking from "./showNetBanking";
import AddCustomer from "./addCustomer";
import ManagerPortal from "./managerPortal";
import CustomerPortal from "./customerPortal";
import CustomerCheques from "./customerCheques";
import CustomerDetails from "./customerDetails";
import NomineeDetails from "./nomineeDetails";
import AddCheque from "./addCheque";
import AddPayee from "./addPayee";
import AddNetDetails from "./addNetDetails";
import CustomerNet from "./customerNet";
import NotAllowed from "./notAllowed";
class MainCompBank extends Component {
  render() {
    const user = auth.getUser();
    return (
      <React.Fragment>
        <Navbar user={user} />
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Route path="/home" component={Home} />
          <Route
            path="/allCustomers"
            render={(props) =>
              user ? (
                user.role === "manager" ? (
                  <ShowCustomers {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/allCheque"
            render={(props) =>
              user ? (
                user.role === "manager" ? (
                  <ShowCheques {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/allNet"
            render={(props) =>
              user ? (
                user.role === "manager" ? (
                  <ShowNetBankking {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/addCustomer"
            render={(props) =>
              user ? (
                user.role === "manager" ? (
                  <AddCustomer {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/viewCheque"
            render={(props) =>
              user ? (
                user.role === "customer" ? (
                  <CustomerCheques {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/addPayee"
            render={(props) =>
              user ? (
                user.role === "customer" ? (
                  <AddPayee {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/netBanking"
            render={(props) =>
              user ? (
                user.role === "customer" ? (
                  <AddNetDetails {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/cheque"
            render={(props) =>
              user ? (
                user.role === "customer" ? (
                  <AddCheque {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/customerDetails"
            render={(props) =>
              user ? (
                user.role === "customer" ? (
                  <CustomerDetails {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/nomineeDetails"
            render={(props) =>
              user ? (
                user.role === "customer" ? (
                  <NomineeDetails {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/viewNet"
            render={(props) =>
              user ? (
                user.role === "customer" ? (
                  <CustomerNet {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/admin"
            render={(props) =>
              user ? (
                user.role === "manager" ? (
                  <ManagerPortal {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route
            path="/viewCheque"
            render={(props) =>
              user ? (
                user.role === "customer" ? (
                  <CustomerPortal {...props} user={user} />
                ) : (
                  <Redirect to="/notAllowed" />
                )
              ) : (
                <Redirect to="/login" />
              )
            }
          />
          <Route path="/notAllowed" component={NotAllowed} />
          <Redirect from="/" to="/home" />
        </Switch>
      </React.Fragment>
    );
  }
}
export default MainCompBank;
